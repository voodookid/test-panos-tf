terraform {
  required_providers {
    panos = {
      source = "PaloAltoNetworks/panos"
      version = "1.10.1"
    }
  }
}


provider "panos" {
  hostname = "192.168.1.254"
  username = "terraform"
  password = "terraform"
}

module "firewall-policy" {
  source = "C:\\Users\\steve\\test-panos-tf\\modules\\denyall"
}



