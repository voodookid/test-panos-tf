terraform {
  required_providers {
    panos = {
      source = "PaloAltoNetworks/panos"
      version = "1.10.1"
    }
  }
}

provider "panos" {
  hostname = "192.168.1.254"
  username = "terraform"
  password = "terraform"
}

resource "panos_security_policy" "example" {
    rule {
        name = "the opposite of secure"
        audit_comment = "Initial config"
        source_zones = ["any"]
        source_addresses = ["any"]
        source_users = ["any"]
        hip_profiles = ["any"]
        destination_zones = ["any"]
        destination_addresses = ["any"]
        applications = ["any"]
        services = ["application-default"]
        categories = ["any"]
        action = "allow"
    }
}